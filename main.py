import config
from imapclient import IMAPClient
from smtplib import SMTP
from email.message import EmailMessage


def send_replies(replies: list):
    with SMTP(host=config.smtp_host, port=config.smtp_port) as smtp:
        smtp.starttls()
        smtp.login(config.smtp_username, config.smtp_password)

        # Go through each address
        for address in replies:
            data = replies[address]

            # Form an email message
            msg = EmailMessage()
            msg.set_content(data["content"])
            msg["Subject"] = data["subject"]
            msg["From"] = config.smtp_from
            msg["To"] = address

            smtp.send_message(msg)


with IMAPClient(host=config.imap_host) as client:
    client.login(config.imap_username, config.imap_password)

    # Get all messages in inbox
    client.select_folder("INBOX")
    messages = client.search(["NOT", "DELETED"])

    replies = {}

    for msgid, data in client.fetch(messages, ["ENVELOPE"]).items():
        envelope = data[b"ENVELOPE"]

        # If there's no subject, fill it with a dummy one
        if envelope.subject:
            subject = envelope.subject.decode()
        else:
            subject = ""

        sender = envelope.sender

        # If there's no sender, we can't reply, so just ignore it
        if not sender:
            continue

        # Form the address of the sender, print it out for debugging purposes
        sender_addr = f"{sender[0].mailbox.decode()}@{sender[0].host.decode()}"
        print(f"Got {subject} from {sender_addr}")

        # Go through the subject to determine if we should give a custom reply
        # (useful for specific projects, topics etc), if not, use default one
        reply = config.default_reply
        for custom_reply in config.custom_replies:
            if custom_reply.lower() in subject.lower():
                reply = config.custom_replies[custom_reply]

        # Set it as a reply
        replies[sender_addr] = reply

    # Send all replies, if we got any
    if replies:
        send_replies(replies)

    # Wipe the messages we just viewed so we don't accidentally re-reply
    # a better way of doing this may be possible, but I'm lazy
    client.delete_messages(messages)

    client.logout()
