smtp_host = ""
smtp_port = 587
smtp_username = ""
smtp_password = ""
smtp_from = smtp_username

imap_host = ""
imap_username = ""
imap_password = ""

default_reply = {"subject": "Out of office notice", "content": "Hello! I'm currently out of office and can't respond to you."}
custom_replies = {"Example Project": {"subject": "Out of office notice", "content": "Hello! I'm currently out of office and can't respond to you. My coworker test@example.com is dealing with Example Project, please email them in the meanwhile."}}
